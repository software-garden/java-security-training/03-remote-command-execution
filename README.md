# Lab 3. Remote Command Execution

This repository demonstrates the attack on Java deserialization. It contains a simplified application that allows users to download data about planets and upload the serialized objects back.

There are three "legitimate" planets to download and one sneaky, questionable planet. The legitimate ones are:

- Mercury
- Venus
- Earth

You can download them for example like this:

``` text
$ java Download earth
Earth:
rO0ABXNyAAZQbGFuZXSgV2hDIHwMJgMABEQABG1hc3NJAAVvcmRlckwABG5hbWV0ABJMamF2YS9sYW5nL1N0cmluZztMAAhuaWNrbmFtZXEAfgABeHB0AAVFYXJ0aHQAC0JsdWUgcGxhbmV0eA==
```

It is possible to re-upload the serialized data with the following command:

``` text
$ echo "rO0ABXNyAAZQbGFuZXSgV2hDIHwMJgMABEQABG1hc3NJAAVvcmRlckwABG5hbWV0ABJMamF2YS9sYW5nL1N0cmluZztMAAhuaWNrbmFtZXEAfgABeHB0AAVFYXJ0aHQAC0JsdWUgcGxhbmV0eA==" | java Upload
Successfully uploaded:
Earth (Blue planet)
```

For security reasons the `Upload` program keeps a log of what was uploaded. At this point it looks like this:

``` text
$ cat planets.log
Thu 22 Apr 14:47:06 CEST 2021: received Earth
```

For simplicity you can pipe the two commands:

``` text
$ java Download mercury | java Upload
Successfully uploaded:
Mercury (Swift planet)
```

The `Upload` program is smart enough to ignore the data that doesn't look like serialized objects. The log should contain new entries:

``` text
$ cat planets.log
Thu 22 Apr 14:47:06 CEST 2021: received Earth
Thu 22 Apr 14:48:19 CEST 2021: received Mercury
```

## The attack

Recently Pluto has been demoted and is not considered a planet anymore by some astronomers. Other astronomers simply can't get over it! So they decided to hack into the solar system database. Try this:

``` text
$ java Download pluto | java Upload
Successfully uploaded:
Pluto >> planets.log; echo 'Consider this a warning' >> you-are-a-victim.log; echo -n '' (Pluto is a planet!)
```

Uh, oh! Something smells here. Have we been hacked already?

``` text
$ cat you-are-a-victim.log
Consider this a warning
```

Apparently yes! This time the attacker only left a warning message, but it could have been much worse. Apparently they are somehow able to execute any arbitrary command. What if they would demote earth and only consider the gas giants to be real planets?

But surely the attack left a trace in the logs:

``` text
$ cat planets.log
Thu 22 Apr 14:47:06 CEST 2021: received Earth
Thu 22 Apr 14:48:19 CEST 2021: received Mercury
Thu 22 Apr 14:51:02 CEST 2021: received Pluto
```

Oh, oh! There seems to be nothing suspicious there, but clearly we have been pwnd here.

![Planet (?) Pluto in silly sunglasses](./pluto.jpg "The revenge of Pluto")

## Your job

Your task is to figure out what's going on here and fix it. The system must accept any name for a planet, but obviously should not execute any commands. Also, the log must show exactly what was uploaded. As it is now, somebody tampered with it to cover their tracks.

Notice that the CI is failing. That's because there are already tests in place to make sure that the attack like this can't happen. At the same time everything else should continue to work as it does.

Fork the repository and make the CI pass. Try to avoid modifying anything else than the Java code. 

This is meant to be a group exercise. Together try to find a most elegant solution.

## Credit

The code and attack technique is adapted from <https://github.com/kojenov/serial/tree/master/5.%20rce>. Most of the credit goes to Alexei Kojenov. We highly recommend watching his presentation about serialization attacks: <https://www.youtube.com/watch?v=t-zVC-CxYjw>
