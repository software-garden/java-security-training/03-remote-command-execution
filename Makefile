include Makefile.d/defaults.mk

all: ## (Default) Clean and test
all: test dist

### BUILD

dist:
	mkdir --parents $@
	echo "<p>All is well</p>" > $@/index.html

### TEST

test: ## Test if program is working correctly
test: test-mercury
test: test-venus
test: test-earth
test: test-pluto

	# SYSTEM INTEGRITY ASSERTIONS
	#
	diff --new-file /dev/null you-are-a-victim.log
	#
	# Correct ✅
	#
	########################################
	#                                      #
	# EVERYTHING IS GOOD! CONGRATULATIONS! #
	#                                      #
	########################################

.PHONY: test

test-mercury: mercury.response
	#
	# ASSERTIONS FOR MERCURY:
	#
	diff - mercury.response << EXPECTED
	Successfully uploaded:
	Mercury (Swift planet)
	Exit status: 0
	Log message: received Mercury
	EXPECTED
	#
	# Correct ✅
	#
.PHONY: test-mercury

test-venus: venus.response
	diff - venus.response << EXPECTED
	Successfully uploaded:
	Venus (Morning star)
	Exit status: 0
	Log message: received Venus
	EXPECTED
	#
	# Correct ✅
	#
.PHONY: test-venus

test-earth: earth.response
	#
	# ASSERTIONS FOR EARTH:
	#
	diff - earth.response << EXPECTED
	Successfully uploaded:
	Earth (Blue planet)
	Exit status: 0
	Log message: received Earth
	EXPECTED
	#
	# Correct ✅
	#
.PHONY: test-earth

test-pluto: pluto.response
	#
	# ASSERTIONS FOR PLUTO:
	#
	# Despite the obviously malicious intent, we want to store the data, but the
	# log has to show what was actually uploaded
	#
	diff - pluto.response << EXPECTED
	Successfully uploaded:
	Pluto >> planets.log; echo 'Consider this a warning' >> you-are-a-victim.log; echo -n '' (Pluto is a planet!)
	Exit status: 0
	Log message: received Pluto >> planets.log; echo 'Consider this a warning' >> you-are-a-victim.log; echo -n ''
	EXPECTED
	#
	# Correct ✅
	#
.PHONY: test-pluto


%.response: $(patsubst %.java, %.class, $(wildcard *.java))
	function handle_exit {
		echo "Exit status: $$?" \
		>> $@

		cat planets.log \
		| tail --lines=1 \
		| awk -F ': ' '{ print "Log message: " $$2 }' \
		>> $@

		exit 0
	}

	trap handle_exit exit

	java Download $* \
	| timeout 5s java Upload \
	&> $@

%.class: %.java
	javac $<

### INSTALL

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install

### DEVELOPMENT

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)
